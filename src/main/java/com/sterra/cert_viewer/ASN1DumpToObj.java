/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sterra.cert_viewer;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import org.bouncycastle.asn1.ASN1Boolean;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Enumerated;
import org.bouncycastle.asn1.ASN1GeneralizedTime;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1UTCTime;
import org.bouncycastle.asn1.BEROctetString;
import org.bouncycastle.asn1.BERSequence;
import org.bouncycastle.asn1.BERSet;
import org.bouncycastle.asn1.BERTaggedObject;
import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DERPrintableString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.DERT61String;
import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.asn1.DERVisibleString;
import org.bouncycastle.util.Strings;
import org.bouncycastle.util.encoders.Hex;

/**
 *
 * @author ray
 */
public class ASN1DumpToObj {

    private static final String TAB = "    ";
    private static final int SAMPLE_SIZE = 32;

    public static String getNameObjByOID(String OID) {

        switch (OID) {

            case "1.2.840.113549.1.1.1":
                return "rsaEncryption";
            case "1.2.840.113549.1.1.11":
                return "sha256WithRSAEncryption";
            case "1.2.840.113549.1.9.1":
                return "E-mail Adress";
            case "2.5.4.3":
                return "CN";
            case "2.5.4.5":
                return "SN";
            case "2.5.4.6":
                return "C";
            case "2.5.4.7":
                return "L";
            case "2.5.4.8":
                return "ST";
            case "2.5.4.10":
                return "O";
            case "2.5.4.11":
                return "OU";
            case "2.5.29.14":
                return "Subject Key Identifier";
            case "2.5.29.15":
                return "Key Usage";
            case "2.5.29.17":
                return "Subject Alternative Name";
            case "2.5.29.19":
                return "Basic Constraints";
            case "2.5.29.31":
                return "CRL Distribution Points";
            case "2.5.29.32":
                return "Certificate Policies";
            case "2.5.29.35":
                return "Authority Key Identifier";
            case "2.5.29.37":
                return "Extended Key Usage";
            case "1.3.6.1.5.5.7.3.1":
                return "serverAuth";
            case "1.3.6.1.5.5.7.3.2":
                return "clientAuth";
            case "1.2.643.2.2.3":
                return "gostR3411-94-with-gostR3410-2001";
            case "1.3.6.1.5.5.7.1.1":
                return "Authority Info Access";
            case "1.3.6.1.4.1.11129.2.4.2":
                return "SCT List";
            case "2.23.140.1.2.1":
                return "domain-validated";
            /*
                case "":
                return "";
            case "":
                return "";
             */
            default:
                return OID;
        }

    }

    private static String calculateAscString(byte[] bytes, int off, int len) {
        StringBuilder buf = new StringBuilder();

        for (int i = off; i != off + len; i++) {
            if (bytes[i] >= ' ' && bytes[i] <= '~') {
                buf.append((char) bytes[i]);
            }
        }

        return buf.toString();
    }

    private static String dumpBinaryDataAsString(String indent, byte[] bytes) {
        String nl = "\n";
        StringBuffer buf = new StringBuffer();

        indent += TAB;

        buf.append(nl);
        for (int i = 0; i < bytes.length; i += SAMPLE_SIZE) {
            if (bytes.length - i > SAMPLE_SIZE) {
                buf.append(indent);
                buf.append(Strings.fromByteArray(Hex.encode(bytes, i, SAMPLE_SIZE)));
                buf.append(TAB);
                buf.append(calculateAscString(bytes, i, SAMPLE_SIZE));
                buf.append(nl);
            } else {
                buf.append(indent);
                buf.append(Strings.fromByteArray(Hex.encode(bytes, i, bytes.length - i)));
                for (int j = bytes.length - i; j != SAMPLE_SIZE; j++) {
                    buf.append("  ");
                }
                buf.append(TAB);
                buf.append(calculateAscString(bytes, i, bytes.length - i));
                buf.append(nl);
            }
        }

        return buf.toString();
    }

    static void _dumpAsString(
            String indent,
            boolean verbose,
            ASN1Primitive obj,
            StringBuffer buf, List<ASN1Object> asn1obj) {
        int deep = 3;
        String nl = "\n";
        if (obj instanceof ASN1Sequence) {
            Enumeration e = ((ASN1Sequence) obj).getObjects();
            String tab = indent + TAB;

            buf.append(indent);

            if (obj instanceof BERSequence) {
                buf.append("BER Sequence");
            } else if (obj instanceof DERSequence) {
                buf.append("DER Sequence");

                asn1obj.add(obj);

            } else {

                asn1obj.add(obj);
                buf.append("Sequence");

            }

            buf.append(nl);

            while (e.hasMoreElements()) {
                Object o = e.nextElement();

                if (o == null || o.equals(DERNull.INSTANCE)) {
                    buf.append(tab);
                    buf.append("NULL");
                    buf.append(nl);
                } else if (o instanceof ASN1Primitive) {
                    _dumpAsString(tab, verbose, (ASN1Primitive) o, buf, asn1obj);
                } else {
                    _dumpAsString(tab, verbose, ((ASN1Encodable) o).toASN1Primitive(), buf, asn1obj);
                }
            }
        } else if (obj instanceof ASN1TaggedObject) {
            String tab = indent + TAB;

            buf.append(indent);

            if (obj instanceof BERTaggedObject) {
                buf.append("BER Tagged [");
            } else {
                buf.append("Tagged [");
            }

            ASN1TaggedObject o = (ASN1TaggedObject) obj;

            buf.append(Integer.toString(o.getTagNo()));
            buf.append(']');

            if (!o.isExplicit()) {
                buf.append(" IMPLICIT ");
            }

            buf.append(nl);

            _dumpAsString(tab, verbose, o.getObject(), buf, asn1obj);
        } else if (obj instanceof ASN1Set) {
            Enumeration e = ((ASN1Set) obj).getObjects();
            String tab = indent + TAB;

            buf.append(String.valueOf(indent));
            if (obj instanceof BERSet) {
                buf.append("BER Set");
            } else if (obj instanceof DERSet) {
                buf.append("DER Set");
            } else {
                buf.append("Set");
            }
            buf.append(nl);

            while (e.hasMoreElements()) {
                Object o = e.nextElement();

                if (o == null) {
                    buf.append(tab);
                    buf.append(0);
                    buf.append(nl);
                } else if (o instanceof ASN1Primitive) {
                    _dumpAsString(tab, verbose, (ASN1Primitive) o, buf, asn1obj);
                } else {
                    _dumpAsString(tab, verbose, ((ASN1Encodable) o).toASN1Primitive(), buf, asn1obj);
                }
            }
        } else if (obj instanceof ASN1OctetString) {
            ASN1OctetString oct = (ASN1OctetString) obj;

            if (obj instanceof BEROctetString) {
                buf.append(String.valueOf(indent) + "BER Constructed Octet String" + "[" + oct.getOctets().length + "] ");
                asn1obj.add(obj);

            } else {
                buf.append(String.valueOf(indent) + "DER Octet String" + "[" + oct.getOctets().length + "] ");
                asn1obj.add(obj);
            }
            if (verbose) {

                buf.append(dumpBinaryDataAsString(String.valueOf(indent), oct.getOctets()));

            } else {

                buf.append(nl);

            }
        } else if (obj instanceof ASN1ObjectIdentifier) {

            buf.append(String.valueOf(indent) + "ObjectIdentifier(" + getNameObjByOID(((ASN1ObjectIdentifier) obj).getId()) + ")" + nl);
            asn1obj.add(obj);

        } else if (obj instanceof ASN1Boolean) {

            buf.append(String.valueOf(indent) + "Boolean(" + ((ASN1Boolean) obj).isTrue() + ")" + nl);
            asn1obj.add(obj);

        } else if (obj instanceof ASN1Integer) {

            buf.append(String.valueOf(indent) + "Integer(" + ((ASN1Integer) obj).getValue() + ")" + nl);
            asn1obj.add(obj);

        } else if (obj instanceof DERBitString) {
            DERBitString bt = (DERBitString) obj;

            buf.append(String.valueOf(indent) + "DER Bit String" + "[" + bt.getBytes().length + ", " + bt.getPadBits() + "] ");
            asn1obj.add(obj);

            if (verbose) {

                buf.append(dumpBinaryDataAsString(String.valueOf(indent), bt.getBytes()));

            } else {
                buf.append(nl);
            }
        } else if (obj instanceof DERIA5String) {

            buf.append(String.valueOf(indent) + "IA5String(" + ((DERIA5String) obj).getString() + ") " + nl);
            asn1obj.add(obj);

        } else if (obj instanceof DERUTF8String) {

            buf.append(String.valueOf(indent) + "UTF8String(" + ((DERUTF8String) obj).getString() + ") " + nl);
            asn1obj.add(obj);

        } else if (obj instanceof DERPrintableString) {

            buf.append(String.valueOf(indent) + "PrintableString(" + ((DERPrintableString) obj).getString() + ") " + nl);
            asn1obj.add(obj);

        } else if (obj instanceof DERVisibleString) {

            buf.append(String.valueOf(indent) + "VisibleString(" + ((DERVisibleString) obj).getString() + ") " + nl);
            asn1obj.add(obj);

        } else if (obj instanceof DERBMPString) {

            buf.append(String.valueOf(indent) + "BMPString(" + ((DERBMPString) obj).getString() + ") " + nl);
            asn1obj.add(obj);

        } else if (obj instanceof DERT61String) {

            buf.append(String.valueOf(indent) + "T61String(" + ((DERT61String) obj).getString() + ") " + nl);
            asn1obj.add(obj);

        } else if (obj instanceof ASN1UTCTime) {
            buf.append(indent + "UTCTime(" + ((ASN1UTCTime) obj).getTime() + ") " + nl);
            asn1obj.add(obj);

        } else if (obj instanceof ASN1GeneralizedTime) {
            buf.append(indent).append("GeneralizedTime(").append(((ASN1GeneralizedTime) obj).getTime()).append(") ").append(nl);
            asn1obj.add(obj);
        } else if (obj instanceof ASN1Enumerated) {
            ASN1Enumerated en = (ASN1Enumerated) obj;

            buf.append(String.valueOf(indent)).append("DER Enumerated(").append(en.getValue()).append(")").append(nl);
            asn1obj.add(obj);

        } else {

            buf.append(String.valueOf(indent) + obj.toString() + nl);
            asn1obj.add(obj);

        }
    }

    /**
     * dump out a DER object as a formatted string, in non-verbose mode.
     *
     * @param obj the ASN1Primitive to be dumped out.
     * @return the resulting string.
     */
    public static List<ASN1Object> dumpAsString(
            Object obj) {
        return dumpAsString(obj, false);
    }

    /**
     * Dump out the object as a string.
     *
     * @param obj the object to be dumped
     * @param verbose if true, dump out the contents of octet and bit strings.
     * @return the resulting string.
     */
    public static List<ASN1Object> dumpAsString(
            Object obj,
            boolean verbose) {
        StringBuffer buf = new StringBuffer();
        List<ASN1Object> asn1obj = new ArrayList<>();

        if (obj instanceof ASN1Primitive) {
            _dumpAsString("", verbose, (ASN1Primitive) obj, buf, asn1obj);
        } else if (obj instanceof ASN1Encodable) {
            _dumpAsString("", verbose, ((ASN1Encodable) obj).toASN1Primitive(), buf, asn1obj);
        } else {
            return null;// "unknown object type " + obj.toString();
        }
        StringBuffer buf1 = new StringBuffer();
        buf1 = buf;
        return asn1obj;
        // return buf.toString();
    }

}
